FROM node:13.3

RUN mkdir -p /srv/app/frontend
WORKDIR /usr/srv/frontend

ADD package.json .
ADD yarn.lock .

RUN yarn install

COPY . .

ENV NUXT_HOST=0.0.0.0
ENV NUXT_PORT=3000
EXPOSE 3000

CMD yarn build
CMD yarn dev
