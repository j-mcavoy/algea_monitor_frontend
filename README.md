# algea_monitor

> Website for monitoring algea

## Project Overview
See Wiki Article: https://gitlab.com/JOHNeMac36/algea_monitor_frontend/-/wikis/Project-Overview

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
